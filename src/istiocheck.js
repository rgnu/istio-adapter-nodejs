const grpc = require('grpc');
const loader = require('./loader');
const { authorization } = loader('authorization.proto');

function main() {
    const client = new authorization.HandleAuthorizationService(
        'localhost:3000',
        grpc.credentials.createInsecure()
    );

    let name = process.argv[2] || 'world';

    client.HandleAuthorization({ instance: { name } }, (err, res) => {
        if (err) return console.error(err);
        console.log('Greeting:', res);
    });
}

main();
