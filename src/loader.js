const path = require('path');
const protoLoader = require('@grpc/proto-loader');
const grpc = require('grpc');

const assetsPath = path.join(__dirname, '..', 'protos');
const protoOptions = {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
};

module.exports = (name) => {
    const proto = path.join(assetsPath, name);
    const def = protoLoader.loadSync(proto, protoOptions);
    return grpc.loadPackageDefinition(def);
}