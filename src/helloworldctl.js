const grpc = require('grpc');
const loader = require('./loader');
const { helloworld } = loader('helloworld.proto');

function main() {
    const client = new helloworld.Greeter(
        'localhost:3000',
        grpc.credentials.createInsecure()
    );

    let user = process.argv[2] || 'world';

    client.sayHello({name: user}, (err, res) => {
        if (err) return console.error(err);
        console.log('Greeting:', res);
    });
}

main();