const loader = require('../../loader');
const proto = loader('authorization.proto');

/**
 * Implements the Istio HandleAuthorization RPC method.
 */
function HandleAuthorization({ cacheTtl, cacheCount, responseMessage, responseCode}) {
    return function HandleAuthorization(call, callback) {
        console.info(`Istio Authorization Handler ${JSON.stringify(call.request)}`);
        callback( null, { 
            status: { code: responseCode, message: responseMessage },
            valid_duration: { seconds: cacheTtl },
            valid_use_count: cacheCount
        });
    }
}

module.exports = (server, options) => {
    options = { cacheTtl: 0, cacheCount: 1, responseMessage: 'OK', responseCode: 0, ...options}
    console.info(`Istio Authorization Service Initialized with %j`, options)
    server.addService(
        proto.authorization.HandleAuthorizationService.service, { 
            HandleAuthorization: HandleAuthorization(options)
        }
    );
}