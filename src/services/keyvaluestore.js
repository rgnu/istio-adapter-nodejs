const loader = require('../loader');
const { keyvaluestore } = loader('keyvaluestore.proto');

const store = {};

/**
 * Implements the KeyValueStore RPC methods.
 */
function Set(call, callback) {
    try {
        store[call.request.key] = call.request.value;
        callback(null, undefined);
    } catch (err) {
        callback(err)
    }
}

function Get(call, callback) {
    try {
        callback(null, { value: store[call.request.key] });
    } catch (err) {
        callback(err)
    }
}

function Delete(call, callback) {
    try {
        delete store[call.request.key];
        callback(null, undefined);
    }  catch (err) {
        callback(err)
    }
}

module.exports = (server) => {
    server.addService(
        keyvaluestore.KeyValueStore.service,
        { Get, Set, Delete }
    );
}