const loader = require('../loader');
const { helloworld } = loader('helloworld.proto');

/**
 * Implements the SayHello RPC method.
 */
function sayHello(call, callback) {
    callback(null, {code: 200, message: 'Hello ' + call.request.name});
}

module.exports = (server) => {
    server.addService(helloworld.Greeter.service, { sayHello });
}