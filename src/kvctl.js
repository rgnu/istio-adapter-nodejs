const grpc = require('grpc');
const loader = require('./loader');
const { keyvaluestore } = loader('keyvaluestore.proto');

const UnknownCommandError = new Error('Command unknown');

const commands = {
    handle: (args, cb) => {
        const cmd = args.shift();
        const client = new keyvaluestore.KeyValueStore(
            'localhost:3000',
            grpc.credentials.createInsecure()
        );
        const handler = commands[cmd] || function (client, args, cb) {
            cb(UnknownCommandError);
        };

        handler(client, args,cb);
    },

    get: (client, args, cb) => {
        const key = args.shift();
        client.Get({ key }, cb);
    },

    set: (client, args, cb) => {
        const key = args.shift();
        const value = args.shift();
        client.Set( { key, value }, cb)
    },

    del: (client, args, cb) => {
        const key = args.shift();
        client.Delete({ key }, cb)
    }
}

function main() {
    const args = process.argv.slice(2); 
    
    commands.handle(args, (err, res) => {
        if (err) return console.error(err.message);
        console.log(res);
    });
}

main();