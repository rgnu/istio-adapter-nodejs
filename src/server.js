const grpc = require('grpc');
const services = require('./services');

const PORT = process.env.PORT || 3000;
const AUTH_CACHE_TTL = parseInt(process.env.AUTH_CACHE_TTL || '0');
const AUTH_CACHE_COUNT = parseInt(process.env.AUTH_CACHE_COUNT || '1');
const AUTH_MESSAGE = process.env.AUTH_MESSAGE || 'OK';
const AUTH_CODE = parseInt(process.env.AUTH_CODE || '0');

/**
 * Starts an RPC server that receives requests for the Greeter service at the
 * sample server port
 */
function main() {
    const server = new grpc.Server();

    services.helloworld(server);
    services.keyvaluestore(server);
    services.istio.authorization(server, {
        cacheTtl: AUTH_CACHE_TTL,
        cacheCount: AUTH_CACHE_COUNT,
        responseMessage: AUTH_MESSAGE,
        responseCode: AUTH_CODE
    });

    console.info(`Server Listen on 0.0.0.0:${PORT}`)
    server.bind(`0.0.0.0:${PORT}`, grpc.ServerCredentials.createInsecure());
    server.start();
}

main();
