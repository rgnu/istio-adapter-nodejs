FROM node:10-alpine
LABEL maintainer="rodrigo@rgnu.com.ar"

ENV HOME=/opt/app NODE_ENV=production PORT=3000 PWD=/opt/app

WORKDIR $HOME

COPY package.json $HOME/

RUN npm install && npm cache clean --force

COPY . $HOME

EXPOSE $PORT

CMD [ "node", "src/server" ]
